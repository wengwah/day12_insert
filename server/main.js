// Loads libraries
const path = require("path");
const express = require("express");
const mysql = require("mysql");
const uuidv4 = require("uuid/v4");

// Create a connection pool
const pool = mysql.createPool({
  host: "localhost", // server
  port: 3306, // server
  user: "fred", // login
  password: "fred", // login
  database: "mydb",
  connectionLimit: 4
});

// Registration ID generation


// SQL statements to create database, schema and tables
// This should be done on the sql database

// SQL statements to create an entries to the database 
// when called
const SQL_INSERT_REGISTRATION = 
" insert into registration( regid, username, email, phone, dob) values(?, ?, ?, ?, ?)";

// Create an instance of express
const app = express();

// Configure the port
const port = process.env.PORT || 3000;

//Request handlers (What does this do?)
app.get("/register", function (req, res) {

  var regid = uuidv4().substring(0,8);
  // Data passed to server
  console.info("regid = %s", regid);
  console.info("username = %s", req.query.username);
  console.info("email = %s", req.query.email);
  console.info("phone = %s", req.query.phone);
  console.info("dob = %s", req.query.dob);

  pool.getConnection(function (err, connection) {
      
    //Connection pool error
    if (err) {
      console.error(">>>> error: ", err);
      res.status(500);
      res.end(JSON.stringify(err));
      return;
    }
    
    //We have a connection
    connection.query(SQL_INSERT_REGISTRATION, 
        [ regid, req.query.username, req.query.email, 
        req.query.phone, req.query.dob ], 
        function (err, result) {
          if (err) {
            console.error(">>>> insert: ", err);
            res.status(500);
            res.end(JSON.stringify(err));
            connection.release();
            return;
          }
    });
    
    connection.release();
    res.status(200);
    res.type("text/html");
    res.send("<h2>Registered</h2>");

  });

});



// Defining the paths...?
app.use(express.static(path.join(__dirname, "../client")));

// app.use("/libs", 
//   express.static(path.join(__dirname, "../bower_components")));

// Start the application
app.listen(port, function() {
  console.log("Web app started at " + port);
});